<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "detalles_de_pedido".
 *
 * @property int $IdPedido
 * @property int $IdProducto
 * @property double $PrecioUnidad
 * @property int $Cantidad
 * @property double $IVA
 * @property double $total
 * @property int $iddetalle
 *
 * @property Pedido $pedido
 * @property Producto $producto
 */
class DetallesDePedido extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'detalles_de_pedido';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IdPedido', 'IdProducto', 'Cantidad'], 'integer'],
            [['PrecioUnidad', 'IVA', 'total'], 'number'],
            [['IdPedido', 'IdProducto'], 'unique', 'targetAttribute' => ['IdPedido', 'IdProducto']],
            [['IdPedido'], 'exist', 'skipOnError' => true, 'targetClass' => Pedido::className(), 'targetAttribute' => ['IdPedido' => 'IdPedido']],
            [['IdProducto'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['IdProducto' => 'IdProducto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdPedido' => 'Id Pedido',
            'IdProducto' => 'Id Producto',
            'PrecioUnidad' => 'Precio Unidad',
            'Cantidad' => 'Cantidad',
            'IVA' => 'Iva',
            'total' => 'Total',
            'iddetalle' => 'Iddetalle',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedido()
    {
        return $this->hasOne(Pedido::className(), ['IdPedido' => 'IdPedido']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Producto::className(), ['IdProducto' => 'IdProducto']);
    }

    /**
     * {@inheritdoc}
     * @return DetallesDePedidoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DetallesDePedidoQuery(get_called_class());
    }
}
