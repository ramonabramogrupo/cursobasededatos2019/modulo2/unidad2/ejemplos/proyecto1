<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ivas".
 *
 * @property int $idiva
 * @property double $iva
 * @property double $total
 * @property int $idpedido
 *
 * @property Pedido $pedido
 */
class Ivas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ivas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iva', 'total'], 'number'],
            [['idpedido'], 'integer'],
            [['iva', 'idpedido'], 'unique', 'targetAttribute' => ['iva', 'idpedido']],
            [['idpedido'], 'exist', 'skipOnError' => true, 'targetClass' => Pedido::className(), 'targetAttribute' => ['idpedido' => 'IdPedido']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idiva' => 'Idiva',
            'iva' => 'Iva',
            'total' => 'Total',
            'idpedido' => 'Idpedido',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedido()
    {
        return $this->hasOne(Pedido::className(), ['IdPedido' => 'idpedido']);
    }
}
